package com.ostapiuk.view;

import com.ostapiuk.services.Service;
import org.hibernate.Session;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;
    private Scanner inputString = new Scanner(System.in);
    private Service service = new Service();
    private Session session = service.getSession();

    public View() {
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("A", "A  - Read all Table");

        menu.put("1", "1  - Table: PlaneType");
        menu.put("11", "11 - Create PlaneType");
        menu.put("12", "12 - Read PlaneType");
        menu.put("13", "13 - Delete PlaneType");

        menu.put("2", "2  - Table: Laptop");
        menu.put("21", "21 - Create Laptop");
        menu.put("22", "22 - Read Laptop");
        menu.put("23", "23 - Update Laptop model");
        menu.put("24", "24 - Delete Laptop");

        menu.put("3", "3  - Table: User");
        menu.put("31", "31 - Create User");
        menu.put("32", "32 - Read User");
        menu.put("33", "33 - Update User name");
        menu.put("34", "34 - Delete User");

        menu.put("Q", "Q  - Exit");

        menuMethods.put("A", this::readAllTable);

        menuMethods.put("11", this::createPlaneType);
        menuMethods.put("12", this::readPlaneType);
        menuMethods.put("14", this::deletePlaneType);

        menuMethods.put("21", this::createPlane);
        menuMethods.put("22", this::readPlane);
        menuMethods.put("23", this::updatePlaneName);
        menuMethods.put("24", this::deletePlane);

        menuMethods.put("31", this::createClient);
        menuMethods.put("32", this::readClient);
        menuMethods.put("33", this::updateClientName);
        menuMethods.put("34", this::deleteClient);
    }

    public void readAllTable() {
        service.readAllTable(session);
    }

    // --------------------PlaneType
    public void createPlaneType() {
        service.createPlaneType(session);
    }

    public void readPlaneType() {
        service.readPlaneType(session);
    }

    public void deletePlaneType() {
        service.deletePlaneType(session);
    }

    // --------------------Plane
    public void createPlane() {
        service.createPlane(session);
    }

    public void readPlane() {
        service.readPlane(session);
    }

    public void updatePlaneName() {
        service.updatePlaneName(session);
    }

    public void deletePlane() {
        service.deletePlane(session);
    }


    // --------------------Client
    public void createClient() {
        service.createClient(session);
    }

    public void readClient() {
        service.readClient(session);
    }

    public void updateClientName() {
        service.updateClientName(session);
    }

    public void deleteClient() {
        service.deleteClient(session);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    private void outputSubMenu(String fig) {
        System.out.println("\nSubMENU:");
        for (String key : menu.keySet()) {
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Please, input your choice: ");
            keyMenu = inputString.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                System.out.println("Please, select menu point.");
                keyMenu = inputString.nextLine().toUpperCase();
            }

            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception ignore) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
