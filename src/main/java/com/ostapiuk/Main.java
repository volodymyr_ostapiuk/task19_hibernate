package com.ostapiuk;

import com.ostapiuk.view.View;

public class Main {

    public static void main(String[] args) {
        View view = new View();
        view.show();
    }
}
