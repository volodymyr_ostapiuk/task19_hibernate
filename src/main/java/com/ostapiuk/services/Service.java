package com.ostapiuk.services;

import com.ostapiuk.airport.ClientEntity;
import com.ostapiuk.airport.PlaneEntity;
import com.ostapiuk.airport.PlaneTypeEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.Properties;
import java.util.Scanner;

public class Service {

    private static SessionFactory sessionFactory;
    private Scanner inputString = new Scanner(System.in);

    static {
        try { // Create the SessionFactory from hibernate.properties
            Properties properties = new Properties();
            properties.load(ClassLoader.getSystemClassLoader().getResourceAsStream("hibernate.properties"));
            Configuration configuration = new Configuration().setProperties(properties)
                    .addAnnotatedClass(PlaneEntity.class)
                    .addAnnotatedClass(PlaneTypeEntity.class)
                    .addAnnotatedClass(ClientEntity.class);
            sessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

//  static {
//    try {
//      sessionFactory = new Configuration().buildSessionFactory();
//    } catch (Throwable ex) {
//      throw new ExceptionInInitializerError(ex);
//    }
//  }

    public Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }

    public void readAllTable(Session session) {
        //read PlaneType
        Query query = session.createQuery("from " + "PlaneTypeEntity");
        System.out.format("\nTable Account --------------------\n");
        System.out.format("%3s %-12s %s\n", "ID", "Name");
        for (Object obj : query.list()) {
            PlaneTypeEntity entity = (PlaneTypeEntity) obj;
            System.out.format("%3s %-12s %s\n", entity.getId(), entity.getName());
        }

        //read Plane
        query = session.createQuery("from " + "PlaneEntity");
        System.out.format("\nTable Laptop -------------------\n");
        System.out.format("%3s %s\n", "ID", "Name");
        for (Object obj : query.list()) {
            PlaneEntity entity = (PlaneEntity) obj;
            System.out.format("%3s %s\n", entity.getId(), entity.getName());
        }

        //read Client
        query = session.createQuery("from " + "ClientEntity");
        System.out.format("\nTable User -------------------\n");
        System.out.format("%3s %s\n", "ID", "Name");
        for (Object obj : query.list()) {
            ClientEntity entity = (ClientEntity) obj;
            System.out.format("%3s %s\n", entity.getId(), entity.getName());
        }
    }

    // --------------------PlaneType
    public void createPlaneType(Session session) {
        System.out.println("Input name for plane:");
        String name = inputString.nextLine();
        session.beginTransaction();
        PlaneTypeEntity entity = new PlaneTypeEntity(name);
        session.save(entity);
        session.getTransaction().commit();
        System.out.println("PlaneTypeEntity created!");
    }

    public void readPlaneType(Session session) {
        Query query = session.createQuery("from " + "PlaneTypeEntity");
        System.out.format("\nTable Account --------------------\n");
        System.out.format("%3s %-12s %s\n", "ID", "Name");
        for (Object obj : query.list()) {
            PlaneTypeEntity entity = (PlaneTypeEntity) obj;
            System.out.format("%3s %-12s %s\n", entity.getId(), entity.getName());
        }
    }

    public void deletePlaneType(Session session) {
        System.out.println("Input login:");
        String login = inputString.nextLine();
        System.out.println("Input password:");
        String password = inputString.nextLine();
        PlaneTypeEntity account = (PlaneTypeEntity) session.load(PlaneTypeEntity.class, login);
        if (account != null) {
            session.beginTransaction();
            Query query = session.createQuery("delete PlaneTypeEntity where login =: code1 and password =: code2");
            query.setParameter("code1", login);
            query.setParameter("code2", password);
            int result = query.executeUpdate();
            session.getTransaction().commit();
            System.out.println("PlaneType delete: " + result);
        } else {
            System.out.println("This PlaneType is wrong!");
        }
    }

    // --------------------Plane
    public void createPlane(Session session) {
        System.out.println("Input laptop model:");
        String model = inputString.nextLine();

        session.beginTransaction();
        PlaneEntity entity = new PlaneEntity(model);
        session.save(entity);
        session.getTransaction().commit();
        System.out.println("Laptop created!");
    }

    public void readPlane(Session session) {
        Query query = session.createQuery("from " + "PlaneEntity");
        System.out.format("\nTable Plane -------------------\n");
        System.out.format("%3s %s\n", "ID", "Model");
        for (Object obj : query.list()) {
            PlaneEntity entity = (PlaneEntity) obj;
            System.out.format("%3s %s\n", entity.getId(), entity.getName());
        }
    }

    public void updatePlaneName(Session session) {
        System.out.println("Input plane name:");
        String model = inputString.nextLine();
        System.out.println("Input new laptop model:");
        String newModel = inputString.nextLine();
        PlaneEntity laptop = (PlaneEntity) session.load(PlaneEntity.class, model);
        if (laptop != null) {
            session.beginTransaction();
            Query query = session.createQuery("update PlaneEntity set model =: code1 where model =: code2");
            query.setParameter("code1", newModel);
            query.setParameter("code2", model);
            int result = query.executeUpdate();
            session.getTransaction().commit();
            System.out.println("Laptop model update: " + result);
        } else {
            System.out.println("This model is wrong!");
        }
    }

    public void deletePlane(Session session) {
        System.out.println("Input plane model:");
        String name = inputString.nextLine();
        PlaneEntity laptop = (PlaneEntity) session.load(PlaneEntity.class, name);
        if (laptop != null) {
            session.beginTransaction();
            Query query = session.createQuery("delete PlaneEntity where name =: code1");
            query.setParameter("code1", name);
            int result = query.executeUpdate();
            session.getTransaction().commit();
            System.out.println("Plane name delete: " + result);
        } else {
            System.out.println("This name is wrong!");
        }
    }


    // --------------------Client
    public void createClient(Session session) {
        System.out.println("Input client name:");
        String name = inputString.nextLine();

        session.beginTransaction();
        ClientEntity entity = new ClientEntity(name);
        session.save(entity);
        session.getTransaction().commit();
        System.out.println("Client created!");
    }

    public void readClient(Session session) {
        Query query = session.createQuery("from " + "ClientEntity ");
        System.out.format("\nTable Client -------------------\n");
        System.out.format("%3s %s\n", "ID", "Name");
        for (Object obj : query.list()) {
            ClientEntity entity = (ClientEntity) obj;
            System.out.format("%3s %s\n", entity.getId(), entity.getName());
        }
    }

    public void updateClientName(Session session) {
        System.out.println("Input client name:");
        String name = inputString.nextLine();
        System.out.println("Input new client name:");
        String newName = inputString.nextLine();
        ClientEntity entity = (ClientEntity) session.load(ClientEntity.class, name);
        if (entity != null) {
            session.beginTransaction();
            Query query = session.createQuery("update ClientEntity set name =: code1 where name =: code2");
            query.setParameter("code1", newName);
            query.setParameter("code2", name);
            int result = query.executeUpdate();
            session.getTransaction().commit();
            System.out.println("Client name update: " + result);
        } else {
            System.out.println("This name is wrong!");
        }
    }

    public void deleteClient(Session session) {
        System.out.println("Input client name:");
        String name = inputString.nextLine();
        ClientEntity entity = (ClientEntity) session.load(ClientEntity.class, name);
        if (entity != null) {
            session.beginTransaction();
            Query query = session.createQuery("delete ClientEntity where name =: code1");
            query.setParameter("code1", name);
            int result = query.executeUpdate();
            session.getTransaction().commit();
            System.out.println("User delete: " + result);
        } else {
            System.out.println("This name is wrong!");
        }
    }
}
